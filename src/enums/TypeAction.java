package enums;

public enum TypeAction {
    ADD_ACTION,
    UPDATE_ACTION,
    DELETE_ACTION
}
