package services;

import interfaces.StatisticPeriodObjet;
import model.Action;
import model.PeriodicEntity;

import java.util.ArrayList;

public abstract class PeriodicalService <T extends PeriodicEntity> extends AbstractService<T> implements StatisticPeriodObjet<T> {
    @Override
    public ArrayList<T> getListObjectsByJour(int year, int mois, int jour) {
        ArrayList<T> listData = new ArrayList<T>();
        list.stream().filter(e-> e.getJour()==jour && e.getAnnee()==year && e.getMois()==mois).forEach((e -> listData.add(e)));
        return listData;
    }

    @Override
    public ArrayList<T> getListObjectsByMois(int annee,int mois) {
        ArrayList<T> listData = new ArrayList<T>();
        list.stream().filter(action-> action.getMois()==mois && action.getAnnee()==annee).forEach((action -> listData.add(action)));
        return listData;
    }

    @Override
    public ArrayList<T> getListObjectsByAnnee(int annee) {
        ArrayList<T> listData = new ArrayList<T>();
        list.stream().filter(action-> action.getAnnee()==annee).forEach((action -> listData.add(action)));
        return listData;
    }

    @Override
    public int getNumberObjectByYear(int annee) {
        return getListObjectsByAnnee(annee).size();
    }

    @Override
    public int getNumberObjectByMonth(int annee, int month) {
        return getListObjectsByMois(annee,month).size();
    }

    @Override
    public int getNumberObjectByDay(int annee, int month, int day) {
        return getListObjectsByJour(annee, month, day).size();
    }
}
