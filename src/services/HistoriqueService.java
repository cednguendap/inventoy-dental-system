package services;
import databaseaccess.DAOActionController;
import databaseaccess.ResultRequest;
import enums.TypeAction;
import interfaces.StatisticPeriodObjet;

import model.Action;
import model.Consultation;
import model.Produit;
import utils.IOContainer;

import java.util.ArrayList;

public class HistoriqueService extends PeriodicalService<Action> {

    public HistoriqueService()
    {
        daoController= new DAOActionController();
    }
    public int getNumberProductAddedByDay(int year,int month,int day)
    {
        return this.getListObjectsByJour(year, month, day)
                .stream()
                .filter(action->action.getTypeAction()== TypeAction.DELETE_ACTION)
                .toArray()
                .length;
    }

    public ArrayList<Action> getActionListByTypeAction(ArrayList<Action> actions,TypeAction actionType)
    {
        ArrayList<Action> listData = new ArrayList<Action>();
        actions.stream().filter(action -> action.getTypeAction().equals(actionType)).forEach((e -> listData.add(e)));
        return listData;
    }

    public ArrayList<Action> getActionListByTypeByDay(int annee,int month,int day,TypeAction type)
    {
        ArrayList<Action> listData = new ArrayList<Action>();
        return getActionListByTypeAction(getListObjectsByJour(annee, month, day),type);
    }

    public ArrayList<Action> getActionListByTypeByMonth(int annee,int month,TypeAction type)
    {
        ArrayList<Action> listData = new ArrayList<Action>();
        return getActionListByTypeAction(getListObjectsByMois(annee, month),type);
    }
    public ArrayList<Action> getActionListByTypeByYear(int annee,TypeAction type)
    {
        ArrayList<Action> listData = new ArrayList<Action>();
        return getActionListByTypeAction(getListObjectsByAnnee(annee),type);
    }

    public int getQuantityByTypeByMonth(int year,int month,TypeAction type)
    {
        return getQuantity(getActionListByTypeByMonth(year,month,type));
    }

    public int getQuantityByTypeByYear(int year,TypeAction type)
    {
        return getQuantity(getActionListByTypeByYear(year,type));
    }

    public int getQuantityByTypeByDay(int year,int month,int day,TypeAction type)
    {
        return getQuantity(getActionListByTypeByDay(year,month,day,type));
    }

    public int getQuantity(ArrayList<Action>list)
    {
        return list
                .stream()
                .mapToInt(action->action.getQuantite())
                .reduce(0,(qteTotal,qte)->qteTotal+qte);
    }
}
