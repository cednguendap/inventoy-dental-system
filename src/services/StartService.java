package services;

import databaseaccess.DataAccess;
import databaseaccess.Requete;
import utils.Constante;
import utils.IOContainer;

public class StartService {
    Requete requete=null;

    public void init()
    {
        requete=DataAccess.connexion(Constante.DB_HOST,Constante.DB_PORT,Constante.DB_NAME,Constante.DB_USERNAME,Constante.DB_PASSWORD);
        if(requete!=null)
        {
            injectRequete();
        }
    }
    protected void injectRequete()
    {
        IOContainer.getAuthServiceInstance().getDaoController().setRequete(requete);
        IOContainer.getConsultationServiceInstance().getDaoController().setRequete(requete);
        IOContainer.getHistoriqueServiceInstance().getDaoController().setRequete(requete);
        IOContainer.getPatientServiceInstance().getDaoController().setRequete(requete);
        IOContainer.getSoinsServiceInstance().getDaoController().setRequete(requete);
        IOContainer.getStockServiceInstance().getDaoController().setRequete(requete);
    }
    public  void loadData()
    {

    }
    public void end()
    {
        DataAccess.closeConnexion(requete);
    }
}
