package services;


import databaseaccess.DAOPatientController;
import databaseaccess.ResultRequest;
import model.Patient;

public class PatientService extends AbstractService<Patient> {

    public  PatientService()
    {
        daoController=new DAOPatientController();
    }
}
