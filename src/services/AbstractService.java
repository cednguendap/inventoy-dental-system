package services;

import databaseaccess.ResultRequest;
import databaseaccess.DAOController;
import interfaces.StatisticCollectionObject;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import model.Consultation;
import model.Entity;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;

public abstract class AbstractService<T> implements StatisticCollectionObject<T> {
    protected ObservableList<T> list = FXCollections.observableArrayList();

    protected DAOController<T>  daoController;

    public AbstractService() {
        list.addListener((ListChangeListener.Change<? extends T> obj) ->handleListener(obj));
    }

    public int countAllObject()
    {
        return list.size();
    }
    public int countAllByField(String field,Object value)
    {
        return this.findByField(field,value).size();
    }
    private  ArrayList<Field> getAllFields(ArrayList<Field> fields, Class<?> type)
    {
        fields.addAll(Arrays.asList(type.getDeclaredFields()));

        if (type.getSuperclass() != null) {
            getAllFields(fields, type.getSuperclass());
        }

        return fields;
    }

    public ArrayList<T> findByField(String field,Object value)  {
        ArrayList<T> vResult=new ArrayList<>();
        ArrayList<Field> fields= list.size()>0?getAllFields(new ArrayList<>(),list.get(0).getClass()):new ArrayList<>();
        try {
            for (T e: list)
            {
                Field fe = fields.stream().filter(f->f.getName().equals(field)).findFirst().get();

                fe.setAccessible(true);

                if(fe.get(e).equals(value))
                {
                    vResult.add(e);
                }
            }
        }
        catch (Exception e){
            e.printStackTrace();
        }

        return vResult;
    }

    public ObservableList<T> getList() {
        return list;
    }

    public DAOController<T> getDaoController() {
        return daoController;
    }

    public ResultRequest loadData()
    {
        ResultRequest<ArrayList<T>> result= daoController.fetchAll();
        if (result.getStatut()!=ResultRequest.OK) return null;
        list.addAll(result.getResult());
        result.setResult(null);
        return result;
    }
    public ResultRequest save(T data)
    {
        ResultRequest<T> result=daoController.save(data);
        if(result.getStatut()==ResultRequest.OK)
        {
            list.add(data);
        }
        return  result;
    }
    public ResultRequest update(T data)
    {
        ResultRequest<T> result=daoController.update(data);
        if(result.getStatut()==ResultRequest.OK)
        {
            int pos=list.indexOf(data);
            if(pos>=0) list.add(pos,data);
        }
        return  result;
    }
    public  ResultRequest delete(T data)
    {
        ResultRequest<T> result=daoController.delete(data);
        if(result.getStatut()==ResultRequest.OK)
        {
            int pos=list.indexOf(data);
            if(pos>=0) list.remove(pos);
        }
        return  result;
    }

    public void handleListener(ListChangeListener.Change<? extends T> obj)
    {
        /*while (obj.next())
        {
            for(T addedItem: obj.getAddedSubList()) daoController.save(addedItem);
            for(T removedItem: obj.getRemoved()) daoController.delete(removedItem);
            if(obj.wasUpdated())
            {
                for(int i=obj.getFrom(); i<obj.getTo(); ++i) daoController.update(list.get(i));
            }
        }*/
    }

}
