package services;


import databaseaccess.DAOConsultationController;
import databaseaccess.ResultRequest;
import interfaces.StatisticPeriodObjet;
import model.Consultation;
import utils.DateUtil;
import utils.IOContainer;

import java.util.ArrayList;

public class ConsultationService extends PeriodicalService<Consultation> {

    public  ConsultationService()
    {
        daoController=new DAOConsultationController();
    }

    public int getDayPrice(int year,int month,int day)
    {
        return getListObjectsByJour(year, month, day)
                .stream()
                .mapToInt(consultation ->consultation.getPrix())
                .reduce(0,(amountTotal,consultationAmount)->amountTotal+consultationAmount);
    }
}
