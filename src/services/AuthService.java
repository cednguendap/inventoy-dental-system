package services;

import databaseaccess.DAOProduitController;
import databaseaccess.DAOUserController;
import databaseaccess.ResultRequest;
import model.User;

public class AuthService {

    private DAOUserController daoController= new DAOUserController();



    private User currentUser=null;

    public User getCurrentUser() {
        return currentUser;
    }

    public DAOUserController getDaoController() {
        return daoController;
    }

    public boolean login(String login, String password)
    {
        currentUser=daoController.connexion(login,password);
        return currentUser!=null;
    }

    public void logout()
    {
        currentUser=null;
    }
}
