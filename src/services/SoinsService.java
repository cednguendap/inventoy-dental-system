package services;

import databaseaccess.DAOSoinsController;
import databaseaccess.ResultRequest;
import model.Soins;

public class SoinsService extends AbstractService<Soins>{
    public SoinsService() {
        daoController=new DAOSoinsController();
    }
}
