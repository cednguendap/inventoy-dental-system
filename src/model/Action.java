package model;

import enums.TypeAction;

public class Action extends PeriodicEntity {

    private Produit produit=new Produit();
    private TypeAction typeAction=TypeAction.ADD_ACTION;
    private int quantite=0;


    public Action(){
        super();
    }

    public Action(int id,TypeAction typeAction,int quantite, int jour,int mois,int annee) {
        super(id,annee,mois,jour);
        this.typeAction=typeAction;
        this.quantite=quantite;
    }

    public Produit getProduit() {
        return produit;
    }

    public void setProduit(Produit produit) {
        this.produit = produit;
    }

    public int getQuantite() {
        return quantite;
    }



    public TypeAction getTypeAction() {
        return typeAction;
    }

    public void setTypeAction(TypeAction typeAction) {
        this.typeAction = typeAction;
    }
}
