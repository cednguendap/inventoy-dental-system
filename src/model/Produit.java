package model;

public class Produit  extends Entity{
    private String nom="";
    private String nomCommercial="";
    private String imageUrl="";
    private int quantite=0;
    private String datePeremption="";
    private String conditionnement="";
    private String fabricant="";

    public Produit() {
        super();
    }

    public Produit(
            int id,
            String nom,
            String nomCommercial,
            String imageUrl,
            int quantite,
            String conditionnement,
            String datePeremption,
            String fabricant
    ) {
        super(id);
        this.nom = nom;
        this.nomCommercial = nomCommercial;
        this.imageUrl = imageUrl;
        this.quantite = quantite;
        this.datePeremption = datePeremption;
        this.fabricant=fabricant;
        this.conditionnement=conditionnement;

    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getNomCommercial() {
        return nomCommercial;
    }

    public void setNomCommercial(String nomCommercial) {
        this.nomCommercial = nomCommercial;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public int getQuantite() {
        return quantite;
    }

    public void setQuantite(int quantite) {
        this.quantite = quantite;
    }

    public String getDatePeremption() {
        return datePeremption;
    }

    public void setDatePeremption(String datePeremption) {
        this.datePeremption = datePeremption;
    }

    public String getFabricant() {
        return fabricant;
    }

    public void setFabricant(String fabricant) {
        this.fabricant = fabricant;
    }

    public String getConditionnement() {
        return conditionnement;
    }

    public void setConditionnement(String conditionnement) {
        this.conditionnement = conditionnement;
    }
}
