package model;

public class PeriodicEntity extends Entity {
    private int jour=0;
    private int mois=0;
    private int annee=0;

    public  PeriodicEntity(){
        super();
    }
    public PeriodicEntity(int id,int annee,int mois,int jour)
    {
        super(id);
        this.annee=annee;
        this.mois=mois;
        this.jour=jour;
    }
    public int getJour() {
        return jour;
    }

    public void setJour(int jour) {
        this.jour = jour;
    }

    public int getMois() {
        return mois;
    }

    public void setMois(int mois) {
        this.mois = mois;
    }

    public int getAnnee() {
        return annee;
    }

    public void setAnnee(int annee) {
        this.annee = annee;
    }

    @Override
    public String toString() {
        return "PeriodicEntity{" +
                "jour=" + jour +
                ", mois=" + mois +
                ", annee=" + annee +
                '}';
    }
}
