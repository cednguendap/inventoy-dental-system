package model;

public abstract class Entity {
    private int id=0;

    public Entity(){};

    public Entity(int id)
    {
        this.id=id;
    }
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
