package model;

import enums.TypeUser;

public class User extends Entity
{
    private String login="";
    private String password="";
    private String nom="";
    private TypeUser typeUser=TypeUser.ADMIN;


    public User() {
        super();
    }

    public User(int id,String login,String password,String nom,TypeUser typeUser) {
        super(id);
        this.login = login;
        this.password=password;
        this.nom=nom;
        this.typeUser = typeUser;
    }


    public String getLogin() {
        return login;
    }

    public String getPassword() {
        return password;
    }

    public String getNom() {
        return nom;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public TypeUser getTypeUser() {
        return typeUser;
    }

    public void setTypeUser(TypeUser typeUser) {
        this.typeUser = typeUser;
    }
}
