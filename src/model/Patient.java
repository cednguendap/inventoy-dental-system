package model;

import enums.TypePatient;

public class Patient extends Entity {
    private String matricule="";
    private String nom="";
    private String prenom="";
    private int age=0;
    private String lieuN="";
    private String tel="";
    private String mail="";
    private String sexe="";
    private String adresse="";
    private TypePatient typePatient=TypePatient.SIMPLE;

    public  Patient()
    {
        super();
    }
    public Patient(
            String matricule,
            String nom,
            String prenom,
            int age,
            String lieuN,
            String tel,
            String mail,
            String adresse,
            TypePatient typePatient,
            String sexe
    ) {
        super(0);
        this.matricule = matricule;
        this.nom = nom;
        this.prenom = prenom;
        this.age = age;
        this.lieuN = lieuN;
        this.tel = tel;
        this.mail = mail;
        this.adresse = adresse;
        this.typePatient = typePatient;
        this.sexe=sexe;
    }

    public String getMatricule() {
        return matricule;
    }

    public void setMatricule(String matricule) {
        this.matricule = matricule;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getLieuN() {
        return lieuN;
    }

    public void setLieuN(String lieuN) {
        this.lieuN = lieuN;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public TypePatient getTypePatient() {
        return typePatient;
    }

    public void setTypePatient(TypePatient typePatient) {
        this.typePatient = typePatient;
    }

    public String getSexe() {
        return sexe;
    }

    public void setSexe(String sexe) {
        this.sexe = sexe;
    }
}
