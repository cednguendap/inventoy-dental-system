package model;

public class Soins extends Entity{
    private String nom="";
    private String description="";
    private int prixContoire=0;
    private int prixAssurer=0;
    private int prixPersonnel=0;

    public  Soins(){
        super();
    }

    public Soins(
            int id,
            String nom,
            String description,
            int prixContoire,
            int prixAssurer,
            int prixPersonnel
    ) {
        super(id);
        this.nom = nom;
        this.description = description;
        this.prixContoire = prixContoire;
        this.prixAssurer = prixAssurer;
        this.prixPersonnel = prixPersonnel;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getPrixContoire() {
        return prixContoire;
    }

    public void setPrixContoire(int prixContoire) {
        this.prixContoire = prixContoire;
    }

    public int getPrixAssurer() {
        return prixAssurer;
    }

    public void setPrixAssurer(int prixAssurer) {
        this.prixAssurer = prixAssurer;
    }

    public int getPrixPersonnel() {
        return prixPersonnel;
    }

    public void setPrixPersonnel(int prixPersonnel) {
        this.prixPersonnel = prixPersonnel;
    }
}
