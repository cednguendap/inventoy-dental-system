package model;

public class Consultation extends PeriodicEntity {
    private Patient patient=new Patient();
    private Soins soins=new Soins();
    private int prix=0;

    public  Consultation(){}

    public Consultation(int id,int prix,int jour,int mois,int annee) {
        super(id,annee,mois,jour);
        this.prix = prix;
    }


    public int getPrix() {
        return prix;
    }

    public void setPrix(int prix) {
        this.prix = prix;
    }

    public Patient getPatient() {
        return patient;
    }

    public Soins getSoins() {
        return soins;
    }

    public void setPatient(Patient patient) {
        this.patient = patient;
    }

    public void setSoins(Soins soins) {
        this.soins = soins;
    }
}
