package ui.layout.dashboard.controllers;

import com.jfoenix.controls.JFXButton;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import ui.controllers.AbstractController;
import utils.IOContainer;
import utils.LoadUIService;

import java.io.IOException;
import java.net.URL;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ResourceBundle;

public class BaseController implements Initializable{
    @FXML
    private AnchorPane paneRight;
    @FXML
    private AnchorPane paneLeft;
    @FXML
    private JFXButton btnDashboard;
    @FXML
    private Label lblAccessLevel;
    @FXML
    private Label lblUsername;
    @FXML
    private JFXButton btnCustomers;
    @FXML
    private JFXButton btnInventoryItem;
    @FXML
    private JFXButton btnAccounts;
    @FXML
    private JFXButton btnDueUpdate;
    @FXML
    private JFXButton btnAdmin;
    @FXML
    private JFXButton btn;
    @FXML
    private Label lblClock;
    @FXML
    private JFXButton btnConsultation;
    @FXML
    private JFXButton btnSells;
    private static String username = "";
    private static String accessLevel = "";
    private AnchorPane newRightPane = null;
    private JFXButton temp = null;
    private JFXButton recover = null;
    private static boolean anchorFlag = false;


    /**
     * This method will resize right pane size
     * relative to it's parent whenever window is resized
     */
    private void autoResizePane() {
        newRightPane.setPrefWidth(paneRight.getWidth());
        newRightPane.setPrefHeight(paneRight.getHeight());
    }

    /**
     * This method will help to set appropriate right pane contents
     * respective to the left pane selection and will make it responsive if
     * any window resize occurs
     * @param URL: main.resources.view file path; scene
     * @throws IOException
     */

    @FXML
    public Object ctrlRightPane(String URL) throws IOException {
        AbstractController controller=null;
            paneRight.getChildren().clear(); //Removing previous nodes

            FXMLLoader loader=new FXMLLoader(getClass().getResource(URL));
            newRightPane = loader.load();

            newRightPane.setPrefHeight(paneRight.getHeight());
            newRightPane.setPrefWidth(paneRight.getWidth());
            System.out.println("Link "+URL);
            paneRight.getChildren().add(newRightPane);
            controller= ((AbstractController)loader.getController());
            controller.setBaseController(this);

            //Listener to monitor any window size change
            paneRight.layoutBoundsProperty().addListener((obs, oldVal, newVal) -> {
                // Some components of the scene will be resized automatically
                autoResizePane();
            });

        return controller;
    }

    /**
     * The method here is universal method for all the navigators from left
     * pane which will identify the selection by user and
     * set the respective right pane FXML
     * @param event
     */

    @FXML
    void btnNavigators(ActionEvent event) {
        borderSelector(event); //Marking selected navigator button

        JFXButton btn = (JFXButton)event.getSource();

        // Getting navigation button label
        String btnText = btn.getText();
        System.out.println("Link text "+btnText);

        // Checking which button is clicked from the map
        // and navigating to respective menu
        try {
            ctrlRightPane(LoadUIService.UI_LINK.get(btnText));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * This method will mark selected navigator
     * from left navigation pane and will remove it if another
     * navition button is clicked.
     * @param event
     */
    private void borderSelector(ActionEvent event) {
        JFXButton btn = (JFXButton)event.getSource();

        if(temp == null) {
            temp = btn; //Saving current button properties
        } else {
            temp.setStyle(""); //Resetting previous selected button properties
            temp = btn; //Saving current button properties
        }

        btn.setStyle("-fx-background-color: #455A64");
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        username=IOContainer.getAuthServiceInstance().getCurrentUser().getNom();
        accessLevel = IOContainer.getAuthServiceInstance().getCurrentUser().getTypeUser().toString();
        lblUsername.setText(username);
        lblAccessLevel.setText(accessLevel);

        //Controling access by checking access level of user
        /*if(accessLevel.equals("Employee")) {
            btnAdmin.setDisable(true);

        }*/

        //Setting Clock within a new Thread
        Runnable clock = new Runnable() {
            @Override
            public void run() {
                runClock();
            }
        };

        Thread newClock = new Thread(clock); //Creating new thread
        newClock.setDaemon(true); //Thread will automatically close on applications closing
        newClock.start(); //Starting Thread

        //Setting DashboardController on RightPane
        try {
            ctrlRightPane("/ui/views/home/fxml/home.fxml");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void runClock() {
        while (true) {
            Platform.runLater(new Runnable() {
                @Override
                public void run() {
                    // Getting the system time in a string
                    String time = LocalTime.now().format(DateTimeFormatter.ofPattern("hh:mm:ss a"));
                    // Setting the time in a label
                    lblClock.setText(time);
                }
            });

            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Upon logging out this method will set log in prompt on
     * screen by closing main application
     */
    @FXML
    private void logOut() {
        Stage current = (Stage)lblUsername.getScene().getWindow();
        try {
            IOContainer.getLoadUIServiceInstance().loadStage(LoadUIService.UI_LINK.get("LOGIN"),current);
            // Setting login window
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
