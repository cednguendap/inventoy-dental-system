package ui.views.customer.controllers;

import enums.TypePatient;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import model.Patient;
import model.Produit;
import ui.controllers.AbstractController;
import utils.IOContainer;
import utils.LoadUIService;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class CustomerController extends AbstractController implements Initializable {

    @FXML
    private TableView<Patient> listCustomerTable;

    @FXML
    private TableColumn<Patient, String> columnID;
    @FXML
    private TableColumn<Patient,String> columnFirstName;
    @FXML
    private TableColumn<Patient,String> columnLastName;
    @FXML
    private TableColumn<Patient,String> columnGender;
    @FXML
    private TableColumn<Patient,String> columnAddress;
    @FXML
    private TableColumn<Patient,String> columnPhone;
    @FXML
    private TableColumn<Patient,String> columnBirthPlace;
    @FXML
    private TableColumn<Patient,String> columnEmail;

    @FXML
    private TableColumn<Patient, TypePatient>columnTypeUser;

    @FXML
    private TableColumn<Patient,Integer> columnAge;

    @FXML
    private Label customerQte;

    public void initialize(URL location, ResourceBundle resources) {

        setView();
    }

    private void setView() {
        System.out.println("List customer "+listCustomerTable);
        System.out.println(IOContainer.getPatientServiceInstance().getList());
        if (listCustomerTable!=null)
        {
            columnID.setCellValueFactory(new PropertyValueFactory<Patient,String>("matricule"));
            columnFirstName.setCellValueFactory(new PropertyValueFactory<Patient,String>("nom"));
            columnLastName.setCellValueFactory(new PropertyValueFactory<Patient,String>("prenom"));
            columnGender.setCellValueFactory(new PropertyValueFactory<Patient,String>("sexe"));
            columnAddress.setCellValueFactory(new PropertyValueFactory<Patient,String>("adresse"));
            columnPhone.setCellValueFactory(new PropertyValueFactory<Patient,String>("tel"));
            columnBirthPlace.setCellValueFactory(new PropertyValueFactory<Patient,String>("lieuN"));
            columnEmail.setCellValueFactory(new PropertyValueFactory<Patient,String>("mail"));
            columnTypeUser.setCellValueFactory(new PropertyValueFactory<Patient,TypePatient>("typePatient"));
            columnAge.setCellValueFactory(new PropertyValueFactory<Patient,Integer>("age"));
            listCustomerTable.getItems().addAll(
                    IOContainer.getPatientServiceInstance().getList()
            );
        }
        else
        {
            customerQte.setText(""+IOContainer.getPatientServiceInstance().getList().size());
        }
    }

    @FXML
    void btnEditModeToggle(ActionEvent event) {

    }


    @FXML
    private void listAllCustomers(ActionEvent event) {
        try {
            baseController.ctrlRightPane(LoadUIService.UI_LINK.get("Customers_List"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @FXML
    void btnDelAction(ActionEvent event) {

    }
    @FXML
    void btnSearchAction(ActionEvent event) {

    }

    @FXML
    void btnAddMode(ActionEvent event) {

    }
    @FXML
    void btnSaveAction(ActionEvent event) {

    }
    @FXML
    void backToHome(ActionEvent event)
    {
        try {
            baseController.ctrlRightPane(LoadUIService.UI_LINK.get("Customers"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
