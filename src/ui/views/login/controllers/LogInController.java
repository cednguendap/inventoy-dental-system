package ui.views.login.controllers;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXCheckBox;
import com.jfoenix.controls.JFXPasswordField;
import com.jfoenix.controls.JFXTextField;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;
import utils.DateUtil;
import utils.IOContainer;
import utils.LoadUIService;

import java.io.IOException;
import java.util.ResourceBundle;

public class LogInController implements Initializable{
    @FXML
    private JFXButton btnLogIn;
    @FXML
    private JFXTextField txtUsername;
    @FXML
    private Label lblWarnUsername;
    @FXML
    private Label lblWarnPassword;
    @FXML
    private JFXPasswordField txtPassword;
    @FXML
    private JFXTextField txtPasswordShown;
    @FXML
    private JFXCheckBox chkPasswordMask;
    @FXML
    private JFXCheckBox chkSaveCredentials;
    private static final String DIALOG_URL = "/ui/dialogs/error_dialog/fxml/dialog.fxml";
    private static final String RED = "-fx-text-fill: red";
    public static String loggerUsername = "";
    public static String loggerAccessLevel = "";

    @FXML
    void ctrlLogInCheck(ActionEvent event)  {
        userLogger();
    }

    @FXML
    void onEnterKey(KeyEvent event) {
        if(event.getCode().equals(KeyCode.ENTER)) {
            userLogger();
        }
    }

    @FXML
    public void chkPasswordMaskAction()
    {
        if (chkPasswordMask.isSelected())
        {
            txtPasswordShown.setText(txtPassword.getText());
            txtPasswordShown.setVisible(true);
            txtPassword.setVisible(false);
        } else {
            txtPassword.setText(txtPasswordShown.getText());
            txtPassword.setVisible(true);
            txtPasswordShown.setVisible(false);
        }
    }

    /**
     * This method will set the previous saved username
     * password if any. In addition this method is responsible
     * for password visibility toggling
     */
    @Override
    public void initialize(java.net.URL location, ResourceBundle resources) {
        IOContainer.getStartServiceInstance().init();
        System.out.println("Connexion a la bd terminet");
    }

    /**
     *
     */
    private void userLogger() {
        //Taking input from the username & password fields
        String username = txtUsername.getText();
        String password;

        //Getting input from the field in which
        //user inputted password.
	    //Note: We have two password field.
	    //One for visible password and another for hidden.
//        if(chkPasswordMask.isSelected())
           // password = txtPasswordShown.getText();
//        else
            password = txtPassword.getText();

        //Checking if any fields were blank
        //If not then we're attempting to connect to DB

        /*if (username.equals("")) {
            lblWarnUsername.setVisible(true);
        } else if(password.equals("")) {
            lblWarnPassword.setVisible(true);
        } else {*/
            if(IOContainer.getAuthServiceInstance().login(username,password))
            {
                System.out.println("Connexion réussi");
                try {
                    IOContainer.getLoadUIServiceInstance().loadStage("LOADING",(Stage)txtPassword.getScene().getWindow());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            else System.out.println("Login ou mot de passe incorrect");
        //}
    }

}
