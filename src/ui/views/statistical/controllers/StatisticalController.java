package ui.views.statistical.controllers;

import com.jfoenix.controls.JFXDatePicker;
import enums.TypeAction;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ComboBox;
import javafx.util.StringConverter;
import model.Action;
import ui.controllers.AbstractController;
import utils.DateUtil;
import utils.IOContainer;
import utils.LoadUIService;

import java.io.IOException;
import java.net.URL;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.ResourceBundle;

import static java.lang.Integer.parseInt;


public class StatisticalController extends AbstractController implements Initializable {

    @FXML
    private ComboBox<Integer> comboYearProductEntry;

    @FXML
    private ComboBox<String> comboMonthProductEntry;

    @FXML
    private JFXDatePicker datePickerProductEntry;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        comboMonthProductEntry.getItems().addAll(
                DateUtil.monthList
        );
        comboYearProductEntry.getItems().addAll(
                2015,
                2016,
                2017,
                2018,
                2019,
                2020,
                2021,
                2022,
                2023,
                2024,
                2025,
                2026,
                2027,
                2028,
                2029,
                2030
        );
        datePickerProductEntry.setConverter(new StringConverter<LocalDate>()
        {
            private DateTimeFormatter dateTimeFormatter=DateTimeFormatter.ofPattern("dd/MM/yyyy");

            @Override
            public String toString(LocalDate localDate)
            {
                if(localDate==null)
                    return "";
                return dateTimeFormatter.format(localDate);
            }

            @Override
            public LocalDate fromString(String dateString)
            {
                if(dateString==null || dateString.trim().isEmpty())
                {
                    return null;
                }
                return LocalDate.parse(dateString,dateTimeFormatter);
            }
        });
    }

    @FXML
    private ArrayList<Action> chooseData(String axe, String value)
    {
        ArrayList<Action> actions=new ArrayList<Action>();
        String[] dateValue=null;
        switch (axe)
        {
            case "year":
                actions=IOContainer.getHistoriqueServiceInstance().getListObjectsByAnnee(parseInt(value));
                break;
            case "month":
                dateValue=value.split("/");
                actions=IOContainer.getHistoriqueServiceInstance().getListObjectsByMois(parseInt(dateValue[0]),parseInt(dateValue[1]));
                break;
            case "day":
                dateValue=value.split("-");
                actions=IOContainer.getHistoriqueServiceInstance().getListObjectsByJour(
                        parseInt(dateValue[2]),
                        parseInt(dateValue[1]),
                        parseInt(dateValue[0])
                );
        }
        return actions;
    }

    @FXML
    void showItemStat(ActionEvent event)
    {
        String value=""+(comboYearProductEntry.getSelectionModel().getSelectedItem()==null? DateUtil.getTodayYear():comboYearProductEntry.getSelectionModel().getSelectedItem());
        String axe="year";
        if(comboMonthProductEntry.getSelectionModel().getSelectedItem()!=null)
        {
            axe="month";
            value+="/"+DateUtil.getMonthPosition(comboMonthProductEntry.getSelectionModel().getSelectedItem());
        }
        if(datePickerProductEntry.getValue()!=null)
        {
            axe="day";
            value=datePickerProductEntry.getValue().getYear()+"-"+datePickerProductEntry.getValue().getMonthValue()+"-"+datePickerProductEntry.getValue().getDayOfMonth();
            System.out.println("Day "+value);
        }
        try {
            StatisticalItemsController controller= (StatisticalItemsController)baseController.ctrlRightPane(LoadUIService.UI_LINK.get("Statistical_item"));
            ArrayList<Action> actions=new ArrayList<>();
            actions.addAll(IOContainer.getHistoriqueServiceInstance().getActionListByTypeAction(chooseData(axe,value),TypeAction.ADD_ACTION));
            actions.addAll(IOContainer.getHistoriqueServiceInstance().getActionListByTypeAction(chooseData(axe,value),TypeAction.DELETE_ACTION));
            System.out.println(actions);
            controller.setData(actions,axe,value);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @FXML
    void btnProceedAction(ActionEvent event)
    {

    }

}
