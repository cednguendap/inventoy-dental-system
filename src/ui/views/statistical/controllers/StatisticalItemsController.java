package ui.views.statistical.controllers;

import enums.TypeAction;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.chart.BarChart;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.XYChart;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.util.Callback;
import model.Action;
import ui.controllers.AbstractController;
import utils.DateUtil;
import utils.IOContainer;
import utils.LoadUIService;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.ResourceBundle;

public class StatisticalItemsController extends AbstractController implements Initializable {
    ArrayList<Action> actions=new ArrayList<>();
    String axe ="";
    String value="";

    @FXML
    TableView<Action> tabeActionList;

    @FXML
    TableColumn<Action,Integer> columnActionID;

    @FXML
    TableColumn<Action,String> columActionName;

    @FXML
    TableColumn<Action,String> columnActionAction;

    @FXML
    TableColumn<Action,Integer> columnActionQte;

    @FXML
    TableColumn<Action,String> columnActionDate;

    @FXML
    LineChart<String,Integer> lineCharData;

    @FXML
    BarChart<String,Integer> barCharData;



    @Override
    public void initialize(URL location, ResourceBundle resources) {
        lineCharData.setVisible(false);
        barCharData.setVisible(false);

        columnActionID.setCellValueFactory(new PropertyValueFactory<Action,Integer>("id"));
        columActionName.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<Action, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<Action, String> param) {
                return new SimpleStringProperty(param.getValue().getProduit().getNom());
            }
        });

        columnActionAction.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<Action, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<Action, String> param) {
                String value="";
                switch (param.getValue().getTypeAction())
                {
                    case ADD_ACTION:
                        value="ADD";
                        break;
                    case DELETE_ACTION:
                        value="REMOVE";
                        break;
                    case UPDATE_ACTION:
                        value="UPDATE";
                        break;
                }
                return new SimpleStringProperty(value);
            }
        });

        columnActionQte.setCellValueFactory(new PropertyValueFactory<Action,Integer>("quantite"));
        columnActionDate.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<Action, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<Action, String> param) {
                return new SimpleStringProperty(param.getValue().getJour()+"/"+param.getValue().getMois()+"/"+param.getValue().getAnnee());
            }
        });

    }

    public void setData(ArrayList<Action> actions,String axe,String value) {
        this.actions = actions;
        this.axe=axe;
        this.value=value;
        setField();
    }

    void setField()
    {
        tabeActionList.getItems().addAll(actions);
        lineCharData.getXAxis().setLabel(axe);
        if(axe=="year") showDataByYear();
        else if(axe=="day") showDataByDay();
        else if(axe=="month") showDataByMonth();
    }
    public void showDataByMonth()
    {
        lineCharData.setVisible(true);
        XYChart.Series addSerie=new XYChart.Series();
        addSerie.setName("entry products");
        XYChart.Series removeSerie=new XYChart.Series();
        addSerie.setName("Release products");
        XYChart.Series allSerie=new XYChart.Series();
        allSerie.setName("All products");
        String[] dataValue=value.split("/");
        for (int i=0;i<31;i++)
        {
            addSerie.getData().add(new XYChart.Data(String.valueOf(i+1),
                    IOContainer.getHistoriqueServiceInstance().getQuantityByTypeByDay(Integer.parseInt(dataValue[0]),Integer.parseInt(dataValue[1]),i+1,TypeAction.ADD_ACTION))
            );
            removeSerie.getData().add(new XYChart.Data(String.valueOf(i+1),
                    IOContainer.getHistoriqueServiceInstance().getQuantityByTypeByDay(Integer.parseInt(dataValue[0]),Integer.parseInt(dataValue[1]),i+1,TypeAction.DELETE_ACTION))
            );

            allSerie.getData().add(new XYChart.Data(String.valueOf(i+1),
                    IOContainer.getHistoriqueServiceInstance().getQuantity(IOContainer.getHistoriqueServiceInstance().getListObjectsByJour(Integer.parseInt(dataValue[0]),Integer.parseInt(dataValue[1]),i+1)))
            );
        }
        lineCharData.getData().addAll(addSerie,removeSerie,allSerie);
    }
    public void showDataByDay()
    {
        barCharData.setVisible(true);
        XYChart.Series addSerie=new XYChart.Series();
        addSerie.setName("Entry products");
        XYChart.Series removeSerie=new XYChart.Series();
        addSerie.setName("Release products");
        XYChart.Series allSerie=new XYChart.Series();
        allSerie.setName("All products");
        String[] dateData=value.split("-");
        addSerie.getData().add(new XYChart.Data("Entry",
                IOContainer.getHistoriqueServiceInstance().getQuantityByTypeByDay(Integer.parseInt(dateData[0]),Integer.parseInt(dateData[1]),Integer.parseInt(dateData[2]),TypeAction.ADD_ACTION))
        );
        removeSerie.getData().add(new XYChart.Data("Release",
                IOContainer.getHistoriqueServiceInstance().getQuantityByTypeByDay(Integer.parseInt(dateData[0]),Integer.parseInt(dateData[1]),Integer.parseInt(dateData[2]),TypeAction.DELETE_ACTION))
        );

        allSerie.getData().add(new XYChart.Data("All",
                IOContainer.getHistoriqueServiceInstance().getQuantity(IOContainer.getHistoriqueServiceInstance().getListObjectsByJour(Integer.parseInt(dateData[0]),Integer.parseInt(dateData[1]),Integer.parseInt(dateData[2]))))
        );
        barCharData.getData().addAll(addSerie,removeSerie,allSerie);
    }
    public void showDataByYear()
    {
        lineCharData.setVisible(true);
        XYChart.Series addSerie=new XYChart.Series();
        addSerie.setName("entry products");
        XYChart.Series removeSerie=new XYChart.Series();
        addSerie.setName("Release products");
        XYChart.Series allSerie=new XYChart.Series();
        allSerie.setName("All products");

        for (int i=0;i<DateUtil.retMonthList.length;i++)
        {
            addSerie.getData().add(new XYChart.Data(DateUtil.retMonthList[i],
                    IOContainer.getHistoriqueServiceInstance().getQuantityByTypeByMonth(Integer.parseInt(value),i+1,TypeAction.ADD_ACTION))
            );
            removeSerie.getData().add(new XYChart.Data(DateUtil.retMonthList[i],
                    IOContainer.getHistoriqueServiceInstance().getQuantityByTypeByMonth(Integer.parseInt(value),i+1,TypeAction.DELETE_ACTION))
            );

            allSerie.getData().add(new XYChart.Data(DateUtil.retMonthList[i],
                    IOContainer.getHistoriqueServiceInstance().getQuantity(IOContainer.getHistoriqueServiceInstance().getListObjectsByMois(Integer.parseInt(value),i+1)))
            );
        }
        lineCharData.getData().addAll(addSerie,removeSerie,allSerie);
    }
    @FXML
    public void btnProceedAction(ActionEvent event) {
    }
    @FXML
    void backToHome(ActionEvent event)
    {
        try {
            baseController.ctrlRightPane(LoadUIService.UI_LINK.get("Statistical"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
