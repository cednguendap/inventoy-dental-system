package ui.views.items.controllers;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import model.Produit;
import ui.controllers.AbstractController;
import utils.LoadUIService;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class InventoryController extends AbstractController {

    @FXML
    private TableView<Produit> listProducts;

    @FXML
    private TableColumn<Produit,Integer>columnItemID;

    @FXML
    private TableColumn<Produit,String> columnItemName;

    @FXML
    private TableColumn<Produit,String> columnItemType;

    @FXML
    private TableColumn<Produit,String> columnItemDescription;

    @FXML
    private TableColumn<Produit,String> columnItemPackaging;

    @FXML
    private TableColumn<Produit,String> columnItemManufacturer;


    public void initialize(URL location, ResourceBundle resources) {

        setView();
    }

    private void setView() {
        /*if(listProducts!=null)
        {
            columnItemID.setCellValueFactory(new PropertyValueFactory<Produit, Integer>("id"));
            columnItemName.setCellValueFactory(new PropertyValueFactory<Produit, String>("nom"));
            columnItemID.setCellValueFactory(new PropertyValueFactory<Produit, Integer>("id"));
            columnItemID.setCellValueFactory(new PropertyValueFactory<Produit, Integer>("id"));
            columnItemID.setCellValueFactory(new PropertyValueFactory<Produit, Integer>("id"));
            columnItemID.setCellValueFactory(new PropertyValueFactory<Produit, Integer>("id"));
            columnItemID.setCellValueFactory(new PropertyValueFactory<Produit, Integer>("id"));

        }*/
    }

    //This method will set navigate between Items
    private void recordNavigator() {

    }

    @FXML
    void listAllItems(ActionEvent event) {
        try {
            baseController.ctrlRightPane(LoadUIService.UI_LINK.get("Items_List"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @FXML
    void btnDelAction(ActionEvent event) {

    }

    @FXML
    void outOfStockList(ActionEvent event) {

    }

    private void listView() {


    }

    @FXML
    void btnAddMode(ActionEvent event) {

    }


    @FXML
    void btnSearchAction(ActionEvent event) {

    }


    @FXML
    void btnSaveAction(ActionEvent event) {

    }
    @FXML
    void backToHome(ActionEvent event)
    {
        try {
            baseController.ctrlRightPane(LoadUIService.UI_LINK.get("Items"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
