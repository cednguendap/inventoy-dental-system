package ui.views.home.controllers;

import com.jfoenix.controls.JFXButton;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.util.Callback;
import model.Consultation;
import model.Produit;
import ui.controllers.AbstractController;
import ui.controllers.DashboardController;
import ui.layout.dashboard.controllers.BaseController;
import utils.DateUtil;
import utils.IOContainer;
import utils.LoadUIService;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;


public class HomeController extends AbstractController implements Initializable{

    @FXML
    private Label lblItemsAmount;

    @FXML
    private Label lblItemsQuantity;

    @FXML
    private Label lblConsultationQuantity;

    @FXML
    private Label lblConsultationAmount;

    @FXML
    private TableView<Produit> tblRecent;

    @FXML
    private TableColumn<Produit,String> columnNames;

    @FXML
    private TableColumn<Produit,String> comsColumnName;

    @FXML
    private TableView<Produit> listAllProductToday;

    @FXML
    private TableColumn<Produit,String> popupColumnNames;

    @FXML
    private TableColumn<Produit,Integer> popupColumnQte;

    @FXML
    private TableView<Consultation> todayConsultationList;

    @FXML
    private TableColumn<Consultation,Integer> columnConsultID;

    @FXML
    private TableColumn<Consultation,String> columnConsultCustomer;

    @FXML
    private TableColumn<Consultation,Integer> columnConsultPrice;



    @Override
    public void initialize(URL location, ResourceBundle resources) {
        setFields();
    }

    private void setFields() {

        columnNames.setCellValueFactory( new PropertyValueFactory<Produit,String>("nom"));

        comsColumnName.setCellValueFactory(new PropertyValueFactory<Produit, String>("nomCommercial"));

        tblRecent.getItems().addAll(
                IOContainer.getStockServiceInstance().getList()
        );

        if(listAllProductToday!=null)
        {
            popupColumnNames.setCellValueFactory( new PropertyValueFactory<Produit,String>("nom"));

            popupColumnQte.setCellValueFactory(new PropertyValueFactory<Produit, Integer>("quantite"));

            listAllProductToday.getItems().addAll(
                    IOContainer.getStockServiceInstance().getList()
            );
        }

        if(todayConsultationList!=null)
        {
            columnConsultID.setCellValueFactory( new PropertyValueFactory<Consultation,Integer>("id"));

            columnConsultCustomer.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<Consultation, String>, ObservableValue<String>>() {
                @Override
                public ObservableValue<String> call(TableColumn.CellDataFeatures<Consultation, String> param) {
                    return new SimpleStringProperty(param.getValue().getPatient().getNom()+" "+param.getValue().getPatient().getPrenom());
                }
            });

            columnConsultPrice.setCellValueFactory( new PropertyValueFactory<Consultation,Integer>("prix"));
            todayConsultationList.getItems().addAll(
                    IOContainer.getConsultationServiceInstance().getList()
            );
        }

        lblConsultationQuantity.setText(""+
                IOContainer.getConsultationServiceInstance().getListObjectsByJour(DateUtil.getTodayYear(),DateUtil.getTodayMonth(),DateUtil.getTodayDay()).size());


        lblConsultationAmount.setText(""+
                IOContainer.getConsultationServiceInstance().getDayPrice(DateUtil.getTodayYear(),DateUtil.getTodayMonth(),DateUtil.getTodayDay())
        );

        lblItemsQuantity.setText(""+
                IOContainer.getHistoriqueServiceInstance().getNumberProductAddedByDay(DateUtil.getTodayYear(),DateUtil.getTodayMonth(),DateUtil.getTodayDay())
        );

        lblItemsAmount.setText(""+
                IOContainer.getHistoriqueServiceInstance().getNumberProductAddedByDay(DateUtil.getTodayYear(),DateUtil.getTodayMonth(),DateUtil.getTodayDay())
        );
    }
    
    @FXML
    void showProducts(ActionEvent event) {
        try {
            baseController.ctrlRightPane(LoadUIService.UI_LINK.get("Home_items"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @FXML
    void showConsultationList(ActionEvent event) {
        try {
            baseController.ctrlRightPane(LoadUIService.UI_LINK.get("Home_consultation"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @FXML
    void backToHome(ActionEvent event)
    {
        try {
            baseController.ctrlRightPane(LoadUIService.UI_LINK.get("Home"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
