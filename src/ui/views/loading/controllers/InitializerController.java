package ui.views.loading.controllers;

import com.jfoenix.controls.JFXProgressBar;
import javafx.concurrent.Task;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import ui.views.login.controllers.LogInController;
import utils.IOContainer;
import utils.LoadUIService;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

/**
 * Author: Afif Al Mamun
 * Written on: 7/24/2018
 * Project: TeslaRentalInventory
 **/
public class InitializerController implements Initializable {

    private static final int THREAD_SLEEP_INTERVAL = 75;
    @FXML
    private JFXProgressBar progressIndicator;
    @FXML
    private Label taskName;
    public String sessionUser = LogInController.loggerUsername; //This firld will hold userName whos currently using the system
                                                //The field is initiated from LogInController Class
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        LoadRecords initializerTask = new LoadRecords();
        progressIndicator.progressProperty().unbind();
       // progressIndicator.progressProperty().bind(t.progressProperty());
        taskName.textProperty().unbind();
        taskName.textProperty().bind(initializerTask.messageProperty());

        new Thread(initializerTask).start();

        //Loading Main Application upon initializer task's succession
        initializerTask.setOnSucceeded(e -> {
            //Closing Current Stage
            //Stage currentSatge = (Stage) taskName.getScene().getWindow();
            //currentSatge.close();
            loadApplication();
        });
    }

    private void loadApplication() {
        //Creating a new stage for main application
        Parent root = null;


        try {
            Stage base=IOContainer.getLoadUIServiceInstance().loadStage("DASHBOARD",(Stage) taskName.getScene().getWindow());
            String css = this.getClass().getResource(LoadUIService.UI_LINK.get("DASBOARD_CSS")).toExternalForm();
            base.getScene().getStylesheets().add(css);
            base.setTitle("Inventory Dental System");
            base.getIcons().add(new Image(LoadUIService.UI_LINK.get("APP_LOGO")));

            base.setMaximized(true);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    class LoadRecords extends Task {
        @Override
        protected Object call() throws Exception {

            //Load patient
            this.updateMessage("Loading patients...");
            IOContainer.getPatientServiceInstance().loadData();
            Thread.sleep(THREAD_SLEEP_INTERVAL);

            //Load soins
            this.updateMessage("Loading soins...");
            IOContainer.getSoinsServiceInstance().loadData();
            Thread.sleep(THREAD_SLEEP_INTERVAL);

            //Load consultation
            this.updateMessage("Loading consultation...");
            IOContainer.getConsultationServiceInstance().loadData();
            Thread.sleep(THREAD_SLEEP_INTERVAL);

            //Load produit
            this.updateMessage("Loading produit...");
            IOContainer.getStockServiceInstance().loadData();
            Thread.sleep(THREAD_SLEEP_INTERVAL);

            //Load historique
            this.updateMessage("Loading historique...");
            IOContainer.getHistoriqueServiceInstance().loadData();
            Thread.sleep(THREAD_SLEEP_INTERVAL);

            //Updating Task Message
            //DashboardController contents
            this.updateMessage("Loading Dashboard Contents...");
            Thread.sleep(THREAD_SLEEP_INTERVAL);

            //Updating Status of the Task
            this.updateMessage("Loading Finished!");
            Thread.sleep(THREAD_SLEEP_INTERVAL);

            return null;
        }
    }
}
