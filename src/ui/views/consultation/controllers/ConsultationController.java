package ui.views.consultation.controllers;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.util.Callback;
import model.Consultation;
import model.Produit;
import sun.util.calendar.CalendarDate;
import ui.controllers.AbstractController;
import utils.IOContainer;

import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.ResourceBundle;


public class ConsultationController extends AbstractController implements Initializable {

    @FXML
    TableView<Consultation> consultationList;

    @FXML
    private TableColumn<Consultation,Integer> columnConsultID;

    @FXML
    private TableColumn<Consultation,String> columnConsultCustomer;

    @FXML
    private TableColumn<Consultation,String> columnConsultSoins;

    @FXML
    private TableColumn<Consultation,String> columnConsultDate;

    @FXML
    private TableColumn<Consultation,Integer> columnConsultPaidAmmount;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        columnConsultID.setCellValueFactory(new PropertyValueFactory<Consultation,Integer>("id"));
        columnConsultCustomer.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<Consultation, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<Consultation, String> param) {
                return new SimpleStringProperty(param.getValue().getPatient().getNom()+" "+param.getValue().getPatient().getPrenom());
            }
        });

        columnConsultSoins.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<Consultation, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<Consultation, String> param) {
                return new SimpleStringProperty(param.getValue().getSoins().getNom());
            }
        });

        columnConsultDate.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<Consultation, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<Consultation, String> param) {
                SimpleDateFormat formatDate=new SimpleDateFormat("dd/mm/yy");
                Calendar date=Calendar.getInstance();
                date.set(param.getValue().getAnnee(),param.getValue().getMois(),param.getValue().getJour());
                return new SimpleStringProperty(formatDate.format(date.getTime()));
            }
        });
        columnConsultPaidAmmount.setCellValueFactory(new PropertyValueFactory<Consultation,Integer>("prix"));
        consultationList.getItems().addAll(
                IOContainer.getConsultationServiceInstance().getList()
        );
    }

    @FXML
    void btnProceedAction(ActionEvent event)
    {

    }

}
