package ui.views.treatment.controllers;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import model.Patient;
import model.Soins;
import ui.controllers.AbstractController;
import utils.IOContainer;
import utils.LoadUIService;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class TreatmentController extends AbstractController implements Initializable {

    @FXML
    TableView<Soins> traitementList;

    @FXML
    TableColumn<Soins, Integer> columnID;

    @FXML
    TableColumn<Soins, String> columnName;
    @FXML
    TableColumn<Soins, String> columnDescription;
    @FXML
    TableColumn<Soins, Integer> columnPrixContoir;
    @FXML
    TableColumn<Soins, Integer> columnPrixAssurer;
    @FXML
    TableColumn<Soins, Integer> columnPrixPersonnel;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        if(traitementList!=null)
        {
            columnID.setCellValueFactory(new PropertyValueFactory<Soins,Integer>("id"));
            columnName.setCellValueFactory(new PropertyValueFactory<Soins,String>("nom"));
            columnDescription.setCellValueFactory(new PropertyValueFactory<Soins,String>("description"));
            columnPrixContoir.setCellValueFactory(new PropertyValueFactory<Soins,Integer>("prixContoire"));
            columnPrixAssurer.setCellValueFactory(new PropertyValueFactory<Soins,Integer>("prixAssurer"));
            columnPrixPersonnel.setCellValueFactory(new PropertyValueFactory<Soins,Integer>("prixPersonnel"));

            traitementList.getItems().addAll(
                    IOContainer.getSoinsServiceInstance().getList()
            );
        }
    }

    @FXML
    void btnEditModeToggle(ActionEvent event)
    {

    }

    @FXML
    void btnAddMode(ActionEvent event)
    {

    }
    @FXML
    void btnSaveAction(ActionEvent event)
    {

    }
    @FXML
    void btnSearchAction(ActionEvent event)
    {

    }

    @FXML
    void btnDelAction(ActionEvent event)
    {

    }



    public void showTraitmentList(ActionEvent event) {
        try {
            baseController.ctrlRightPane(LoadUIService.UI_LINK.get("Treatment_list"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    @FXML
    void backToHome(ActionEvent event)
    {
        try {
            baseController.ctrlRightPane(LoadUIService.UI_LINK.get("Treatment"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
