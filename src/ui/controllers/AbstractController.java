package ui.controllers;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import ui.layout.dashboard.controllers.BaseController;
import utils.LoadUIService;

import java.io.IOException;

public class AbstractController {
    protected BaseController baseController;

    public void setBaseController(BaseController baseController) {
        this.baseController = baseController;
    }


}
