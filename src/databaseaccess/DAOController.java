/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package databaseaccess;

import databaseaccess.Requete;
import databaseaccess.ResultRequest;
import model.User;

import java.util.ArrayList;
import java.util.Map;

import static java.lang.Integer.parseInt;

/**
 *
 * @author NGUENDAP
 */
public abstract class DAOController<T> 
{
    protected Requete requete;
    protected String tableName="";
    public void setRequete(Requete requete)
    {
        this.requete=requete;
    }

    public ResultRequest<ArrayList<T>> fetchAll() {
        ResultRequest result=requete.fetchAll("SELECT * FROM "+tableName);
        if(result.getStatut()!=ResultRequest.OK  || result.getResult()==null) return null;
        ArrayList<Map> resultData=(ArrayList<Map>) result.getResult();
        ArrayList<T> entities=new ArrayList<>();
        for(Map entite:resultData)
        {
            entities.add(this.hydrate(entite));
        }
        System.out.println("Fin load dans la table "+tableName);
        result.setResult(entities);
        return result;
    }

    public abstract ResultRequest save(T data);
    public abstract ResultRequest update(T data);
    public abstract ResultRequest delete(T data);
    public abstract ResultRequest fetchOne(T data);

    public abstract  T hydrate(Map obj);
}
