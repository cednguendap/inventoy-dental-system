package databaseaccess;

import enums.TypeAction;
import model.Action;
import model.Consultation;
import model.Produit;
import utils.IOContainer;

import java.util.ArrayList;
import java.util.Map;

import static java.lang.Integer.parseInt;

public class DAOActionController extends DAOController<Action>{

    public  DAOActionController()
    {
        tableName="action";
    }

    @Override
    public ResultRequest save(Action data) {
        return requete.add("INSERT INTO action (idproduit,type,quantite,jour,moi,annee)"
                + "VALUES ('"+data.getProduit().getId()+"','"+data.getTypeAction()+"','" +data.getQuantite()+
                "','"+data.getJour()+"','"+data.getMois()+"','"+data.getAnnee()+"')");
    }

    @Override
    public ResultRequest update(Action data) {
        return null;
    }

    @Override
    public ResultRequest delete(Action data) {
        return requete.delete("DELETE FROM action WHERE idaction="+data.getId());
    }

    @Override
    public ResultRequest fetchOne(Action data) {
        return null;
    }

    @Override
    public Action hydrate(Map obj) {
        Action action= new Action(
                parseInt(obj.get("idaction").toString()),
                TypeAction.valueOf(obj.get("type").toString()),
                parseInt(obj.get("quantite").toString()),
                parseInt(obj.get("jour").toString()),
                parseInt(obj.get("moi").toString()),
                parseInt(obj.get("annee").toString())
        );
        ArrayList<Produit> produits=IOContainer.getStockServiceInstance().findByField("id",parseInt(obj.get("idproduit").toString()));
        action.setProduit(produits.size()>0?produits.get(0):null);

        return action;
    }
}
