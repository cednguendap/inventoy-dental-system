package databaseaccess;

import model.Produit;

import java.util.Map;

import static java.lang.Integer.parseInt;

public class DAOProduitController extends DAOController<Produit> {
    public  DAOProduitController()
    {
        tableName="produit";
    }

    @Override
    public ResultRequest save(Produit data) {
        return requete.add("INSERT INTO produit (nom,nomcommercial,image,quantite,dateperemption,conditionnement,fabricant)"
                + "VALUES ('"+data.getNom()+"','"+data.getNomCommercial()+"','" +data.getImageUrl()+
                "','"+data.getQuantite()+"','"+data.getDatePeremption()+"','"+data.getConditionnement()+"','"+data.getFabricant()+"')");
    }

    @Override
    public ResultRequest update(Produit data) {
        return null;
    }

    @Override
    public ResultRequest delete(Produit data) {
        return requete.delete("DELETE FROM produit WHERE idProduit="+data.getId());
    }

    @Override
    public ResultRequest fetchOne(Produit data) {
        return null;
    }

    @Override
    public Produit hydrate(Map obj) {
        return new Produit(
                parseInt(obj.get("idproduit").toString()),
                obj.get("nom").toString(),
                obj.get("nomcommercial").toString(),
                obj.get("image").toString(),
                parseInt(obj.get("quantite").toString()),
                obj.get("conditionnement").toString(),
                obj.get("dateperemption").toString(),
                obj.get("fabricant").toString()
        );
    }
}
