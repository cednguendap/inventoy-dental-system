/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package databaseaccess;

import enums.TypeUser;

import static java.lang.Integer.parseInt;

import java.util.Map;

import model.User;

/**
 *
 * @author NGUENDAP
 */
public class DAOUserController extends DAOController<User>
{
    public  DAOUserController()
    {
        tableName="utilisateur";
    }

    public User connexion(String login,String password)
    {
        ResultRequest result=requete.fetchOne("SELECT * FROM utilisateur WHERE login='"+login+"' AND password='"+password+"'");
        if(result.getStatut()!=ResultRequest.OK) return null;
        if(result.getResult()==null) return null;

        return hydrate((Map) result.getResult());
    }

    @Override
    public ResultRequest save(User data) {
        return requete.add("INSERT INTO user (login,password,nom,typeuser)"
                + "VALUES ('"+data.getLogin()+"','"+data.getPassword()+"','"+data.getNom()+"','"+data.getTypeUser()+"')");
    }

    @Override
    public ResultRequest update(User data) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public ResultRequest delete(User data) {
        return requete.delete("DELETE FROM utilisateur WHERE idUtilisateur="+data.getId());
    }

    @Override
    public User hydrate(Map obj)
    {
        System.out.println(obj);
        return new User(
                parseInt(obj.get("idutilisateur").toString()),
                (String)obj.get("login"),
                (String)obj.get("password"),
                (String)obj.get("nom"),
                TypeUser.valueOf(obj.get("typeuser").toString())
        );
    }


    @Override
    public ResultRequest fetchOne(User data) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
