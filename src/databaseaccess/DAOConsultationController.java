package databaseaccess;

import model.Consultation;
import model.Patient;
import model.Produit;
import model.Soins;
import utils.IOContainer;

import java.util.ArrayList;
import java.util.Map;

import static java.lang.Integer.parseInt;

public class DAOConsultationController extends DAOController<Consultation>{

    public  DAOConsultationController()
    {
        tableName="consultation";
    }

    @Override
    public ResultRequest save(Consultation data) {
        return requete.add("INSERT INTO consultation (matricule,idsoins,jour,moi,annee,prix)"
                + "VALUES ('"+data.getPatient().getMatricule()+"','"+data.getSoins().getId()+"','" +
                "','"+data.getJour()+"','"+data.getMois()+"','"+data.getAnnee()+"','"+data.getPrix()+"')");
    }

    @Override
    public ResultRequest update(Consultation data) {
        return null;
    }

    @Override
    public ResultRequest delete(Consultation data) {
        return requete.delete("DELETE FROM patient WHERE idconsultation="+data.getId());
    }

     @Override
    public ResultRequest fetchOne(Consultation data) {
        return null;
    }

    @Override
    public Consultation hydrate(Map obj) {
        Consultation consultation= new Consultation(
                parseInt(obj.get("idconsultation").toString()),
                parseInt(obj.get("prix").toString()),
                parseInt(obj.get("jour").toString()),
                parseInt(obj.get("moi").toString()),
                parseInt(obj.get("annee").toString())
        );

        ArrayList<Patient> patients =IOContainer.getPatientServiceInstance().findByField("matricule",obj.get("matricule").toString());

        consultation.setPatient(patients.size()>0?patients.get(0):null);
        ArrayList<Soins> soins = IOContainer.getSoinsServiceInstance().findByField("id",parseInt(obj.get("idsoins").toString()));
        consultation.setSoins(soins.size()>0?soins.get(0):null);
        return consultation;
    }
}
