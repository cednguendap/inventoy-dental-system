package databaseaccess;

import model.Soins;

import java.util.Map;

import static java.lang.Integer.parseInt;

public class DAOSoinsController extends DAOController<Soins>{
    public  DAOSoinsController()
    {
        tableName="soins";
    }

    @Override
    public ResultRequest save(Soins data) {
        return requete.add("INSERT INTO soins (nom,description,prixcontroire,prixassurer,prixpersonnel)"
                + "VALUES ('"+data.getNom()+"','"+data.getDescription()+"','" +data.getPrixContoire()+
                "','"+data.getPrixAssurer()+"','"+data.getPrixPersonnel()+"')");
    }

    @Override
    public ResultRequest update(Soins data) {
        return null;
    }

    @Override
    public ResultRequest delete(Soins data) {
        return requete.delete("DELETE FROM soins WHERE idsoins="+data.getId());
    }

    @Override
    public ResultRequest fetchOne(Soins data) {
        return null;
    }

    @Override
    public Soins hydrate(Map obj) {
        return new Soins(
                parseInt(obj.get("idsoins").toString()),
                obj.get("nom").toString(),
                obj.get("description").toString(),
                parseInt(obj.get("prixcontoire").toString()),
                parseInt(obj.get("prixassurer").toString()),
                parseInt(obj.get("prixpersonnel").toString())
        );
    }
}
