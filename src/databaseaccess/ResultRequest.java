/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package databaseaccess;

import java.util.ArrayList;

/**
 *
 * @author NGUENDAP
 */
public class ResultRequest<T> {
    private String action;
    private T result;
    private int statut;
    private String descriptionResult;    
    public static int ERROR=-1;
    public static int OK=0;
    

    public ResultRequest(String action, T result, String descriptionResult,int statut) {
        this.action = action;
        this.result = result;
        this.descriptionResult = descriptionResult;
        this.statut=statut;
    }

    public void setStatut(int statut) {
        this.statut = statut;
    }

    public int getStatut() {
        return statut;
    }

    public String getAction() {
        return action;
    }

    public T getResult() {
        return result;
    }

    public String getDescriptionResult() {
        return descriptionResult;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public void setResult(T result) {
        this.result = result;
    }

    public void setDescriptionResult(String descriptionResult) {
        this.descriptionResult = descriptionResult;
    }
    
    
    
}
