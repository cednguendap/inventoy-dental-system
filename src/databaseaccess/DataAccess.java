/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package databaseaccess;

import java.sql.DriverManager;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author NGUENDAP
 */
public class DataAccess {
    public static Requete connexion(String host,int port,String database, String user, String password)
    {
        Requete requete=null;
        String stringConnexion="jdbc:mysql://"+host+":"+port+"/"+database+"?user="+user+"&password="+password+"&serverTimezone=UTC";
        try {
            Class.forName("com.mysql.cj.jdbc.Driver").newInstance();
            requete=new Requete(DriverManager.getConnection(stringConnexion));
        } catch (Exception ex) {
            Logger.getLogger(DataAccess.class.getName()).log(Level.SEVERE, null, ex);
        }
        System.out.println(requete.toString());
        return requete;
    }
    public static void closeConnexion(Requete requete)
    {
        
    }
    public static Requete liteConnexion()
    {
        System.out.println("Try to connect to sqlite");
        Requete requete=null;
        System.out.println(DataAccess.class.getResource("./../ressources/inventory.db"));
        String stringConnexion="jdbc:sqlite:../ressources/inventory.db";
        try {
            requete=new Requete(DriverManager.getConnection(stringConnexion));
            System.out.println("Dans le try de connexion");
            System.out.println(requete.toString());
        } catch (Exception ex) {
            Logger.getLogger(DataAccess.class.getName()).log(Level.SEVERE, null, ex);
        }
        System.out.println(requete.toString());
        return requete;
    }
}
