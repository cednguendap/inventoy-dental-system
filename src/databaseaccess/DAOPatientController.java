package databaseaccess;

import enums.TypePatient;
import enums.TypeUser;
import model.Patient;
import model.User;

import java.util.Map;

import static java.lang.Integer.parseInt;

public class DAOPatientController extends DAOController<Patient>{

    public  DAOPatientController()
    {
        tableName="patient";
    }

    @Override
    public ResultRequest save(Patient data) {
        return requete.add("INSERT INTO patient (matricule,nom,prenom,age,lieun,telephone,mail,sexe,adresse,typeclient)"
                + "VALUES ('"+data.getMatricule()+"','"+data.getNom()+"','" +data.getPrenom()+
                "','"+data.getAge()+"','"+data.getLieuN()+"','"+data.getTel()+"','"+data.getMail()+
                "','"+data.getSexe()+"','"+data.getAdresse()+"','"+data.getTypePatient()+"')");
    }

    @Override
    public ResultRequest update(Patient data) {
        return null;
    }

    @Override
    public ResultRequest delete(Patient data) {
        return requete.delete("DELETE FROM patient WHERE matricule="+data.getMatricule());
    }

    @Override
    public ResultRequest fetchOne(Patient data) {
        return null;
    }

    @Override
    public Patient hydrate(Map obj) {
        return new Patient(
                obj.get("matricule").toString(),
                obj.get("nom").toString(),
                obj.get("prenom").toString(),
                parseInt(obj.get("age").toString()),
                obj.get("lieun").toString(),
                obj.get("telephone").toString(),
                obj.get("mail").toString(),
                obj.get("adresse").toString(),
                TypePatient.valueOf(obj.get("typeclient").toString()),
                obj.get("sexe").toString()
        );
    }
}
