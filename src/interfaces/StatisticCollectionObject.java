package interfaces;

public interface StatisticCollectionObject<T> {
    int countAllObject();
    int countAllByField(String field,Object value);
}
