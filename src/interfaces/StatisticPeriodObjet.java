package interfaces;

import databaseaccess.ResultRequest;
import model.Action;

import java.util.ArrayList;

public interface StatisticPeriodObjet<T> {

    ArrayList<T> getListObjectsByJour(int anne,int mois,int jour);

    ArrayList<T> getListObjectsByMois(int annee ,int mois);

    ArrayList<T> getListObjectsByAnnee(int annee);

    int getNumberObjectByYear(int annee);

    int getNumberObjectByMonth(int annee,int month);

    int getNumberObjectByDay(int annee,int month,int day);
}
