package utils;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class DateUtil {
    public static String[] monthList=new String[]
            {
                    "january",
                    "febuary",
                    "march",
                    "april",
                    "may",
                    "june",
                    "jully",
                    "august",
                    "september",
                    "october",
                    "november",
                    "december"
            };

    public static String[] retMonthList=new String[]
            {
                    "Jan",
                    "Feb",
                    "Mar",
                    "Apr",
                    "May",
                    "Jun",
                    "Jul",
                    "Aug",
                    "Sep",
                    "Oct",
                    "Nov",
                    "Dev"
            };

    public static int getTodayDay()
    {
        return Calendar.getInstance().get(Calendar.DAY_OF_MONTH);
    }
    public static int getTodayMonth()
    {
        return Calendar.getInstance().get(Calendar.MONTH);
    }
    public static int getTodayYear()
    {
        return Calendar.getInstance().get(Calendar.YEAR);
    }

    public static int getMonthPosition(String month)
    {
        for(int i=0;i<=DateUtil.monthList.length;i++)
        {
            if(DateUtil.monthList[i]==month) return i+1;
        }
        return -1;
    }
}
