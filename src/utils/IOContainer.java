package utils;

import services.*;

public class IOContainer {
    private static volatile HistoriqueService historiqueServiceInstance = null;
    private static volatile ConsultationService consultationServiceInstance = null;
    private static volatile PatientService patientServiceInstance=null;
    private static volatile StockService stockServiceInstance=null;
    private static volatile AuthService authServiceInstance=null;
    private static volatile SoinsService soinsServiceInstance=null;
    private static volatile StartService startServiceInstance=null;
    private static volatile LoadUIService loadUIServiceInstance=null;

    public final static HistoriqueService getHistoriqueServiceInstance() {
        if (IOContainer.historiqueServiceInstance == null) {
            synchronized(HistoriqueService.class) {
                if (IOContainer.historiqueServiceInstance == null) {
                    IOContainer.historiqueServiceInstance = new HistoriqueService();
                }
            }
        }
        return IOContainer.historiqueServiceInstance;
    }

    public final static ConsultationService getConsultationServiceInstance() {
        if (IOContainer.consultationServiceInstance == null) {
            synchronized(ConsultationService.class) {
                if (IOContainer.consultationServiceInstance == null) {
                    IOContainer.consultationServiceInstance = new ConsultationService();
                }
            }
        }
        return IOContainer.consultationServiceInstance;
    }

    public final static PatientService getPatientServiceInstance() {
        if (IOContainer.patientServiceInstance == null) {
            synchronized(PatientService.class) {
                if (IOContainer.patientServiceInstance == null) {
                    IOContainer.patientServiceInstance = new PatientService();
                }
            }
        }
        return IOContainer.patientServiceInstance;
    }

    public final static StockService getStockServiceInstance() {
        if (IOContainer.stockServiceInstance == null) {
            synchronized(StockService.class) {
                if (IOContainer.stockServiceInstance == null) {
                    IOContainer.stockServiceInstance = new StockService();
                }
            }
        }
        return IOContainer.stockServiceInstance;
    }

    public final static AuthService getAuthServiceInstance() {
        if (IOContainer.authServiceInstance == null) {
            synchronized(StockService.class) {
                if (IOContainer.authServiceInstance == null) {
                    IOContainer.authServiceInstance = new AuthService();
                }
            }
        }
        return IOContainer.authServiceInstance;
    }

    public final static SoinsService getSoinsServiceInstance() {
        if (IOContainer.soinsServiceInstance == null) {
            synchronized(SoinsService.class) {
                if (IOContainer.soinsServiceInstance == null) {
                    IOContainer.soinsServiceInstance = new SoinsService();
                }
            }
        }
        return IOContainer.soinsServiceInstance;
    }

    public final static StartService getStartServiceInstance() {
        if (IOContainer.startServiceInstance == null) {
            synchronized(SoinsService.class) {
                if (IOContainer.startServiceInstance == null) {
                    IOContainer.startServiceInstance = new StartService();
                }
            }
        }
        return IOContainer.startServiceInstance;
    }

    public final static LoadUIService getLoadUIServiceInstance() {
        if (IOContainer.loadUIServiceInstance == null) {
            synchronized(SoinsService.class) {
                if (IOContainer.loadUIServiceInstance == null) {
                    IOContainer.loadUIServiceInstance = new LoadUIService();
                }
            }
        }
        return IOContainer.loadUIServiceInstance;
    }
}
