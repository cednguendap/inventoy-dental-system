package utils;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class LoadUIService {
    public static Map<String,String> UI_LINK= new HashMap<>();

    public LoadUIService()
    {
        UI_LINK.put("LOADING","/ui/views/loading/fxml/initializer.fxml");
        UI_LINK.put("DASHBOARD", "/ui/layout/dashboard/fxml/dashboard.fxml");
        UI_LINK.put("DASBOARD_CSS", "/ui/layout/dashboard/css/base.css");
        UI_LINK.put("APP_LOGO","/ui/icons/Logo.png");
        UI_LINK.put("LOGIN", "/ui/views/login/fxml/login.fxml");

        UI_LINK.put("Items", "/ui/views/items/fxml/items.fxml");
        UI_LINK.put("Items_List", "/ui/views/items/fxml/itemslist.fxml");
        UI_LINK.put("Customers", "/ui/views/customer/fxml/customer.fxml");
        UI_LINK.put("Customers_List", "/ui/views/customer/fxml/customer_list.fxml");
        UI_LINK.put("Treatment", "/ui/views/treatment/fxml/treatment.fxml");
        UI_LINK.put("Treatment_list", "/ui/views/treatment/fxml/treatment_list.fxml");

        UI_LINK.put("Consultation", "/ui/views/consultation/fxml/consultation.fxml");
        UI_LINK.put("Home", "/ui/views/home/fxml/home.fxml");
        UI_LINK.put("Home_items", "/ui/views/home/fxml/home_items.fxml");
        UI_LINK.put("Home_consultation", "/ui/views/home/fxml/home_consultations.fxml");
        UI_LINK.put("Statistical", "/ui/views/statistical/fxml/statistics.fxml");
        UI_LINK.put("Statistical_item", "/ui/views/statistical/fxml/itemsstat.fxml");

        UI_LINK.put("Administrative", "/ui/views/admin/fxml/administrator.fxml");

        UI_LINK.put("Accounts", "/main/resources/view/accounts.fxml");
        UI_LINK.put("Update Due", "/main/resources/view/dueupdate.fxml");


    }

    public void load(String link)
    {

    }
    public Stage loadStage(String key, Stage stage) throws IOException {
        System.out.println(UI_LINK.get(key));
        Stage nStage=new Stage();
        stage.close();
        Parent element = FXMLLoader.load(getClass().getResource(UI_LINK.get(key)));
        Scene s = new Scene(element);
        nStage.setScene(s);
        nStage.show();
        return  nStage;
    }
}
